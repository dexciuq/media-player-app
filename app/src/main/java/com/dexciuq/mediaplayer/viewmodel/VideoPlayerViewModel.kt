package com.dexciuq.mediaplayer.viewmodel

import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

class VideoPlayerViewModel : ViewModel() {

    private val _videoUri = MutableLiveData<Uri>()
    val videoUri: LiveData<Uri> = _videoUri

    private val _isPlaying = MutableLiveData<Boolean>()
    val isPlaying: LiveData<Boolean> = _isPlaying

    private val _currentTime = MutableLiveData<Int>()
    val currentTime: LiveData<Int> = _currentTime

    private val _duration = MutableLiveData<Int>()
    val duration: LiveData<Int> = _duration

    private var mediaPlayer: MediaPlayer? = null
    private var scheduledExecutor: ScheduledExecutorService? = null

    private fun startUpdateCurrentTime() {
        stopUpdateCurrentTime()
        scheduledExecutor = Executors.newSingleThreadScheduledExecutor()
        scheduledExecutor?.scheduleAtFixedRate({
                mediaPlayer?.let {
                    _currentTime.postValue(it.currentPosition)
                }
            }, 0, 10, TimeUnit.MILLISECONDS)
    }

    private fun stopUpdateCurrentTime() {
        scheduledExecutor?.shutdownNow()
        scheduledExecutor = null
    }

    fun setVideoFile(uri: Uri) {
        _videoUri.value = uri
        _currentTime.value = 0
        _isPlaying.value = false
    }

    fun setMediaPlayer(player: MediaPlayer?) {
        releaseMediaPlayer()
        mediaPlayer = player
        _duration.value = player?.duration
    }

    fun complete() {
        pauseVideo()
        _currentTime.value = 0
    }

    fun updateCurrentTime(currentTime: Int) {
        _currentTime.value = currentTime
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun playVideo() {
        mediaPlayer?.apply {
            seekTo(_currentTime.value?.toLong() ?: 0, MediaPlayer.SEEK_CLOSEST)
            start()
        }
        _isPlaying.value = true
        startUpdateCurrentTime()
    }

    fun pauseVideo() {
        mediaPlayer?.pause()
        _isPlaying.value = false
        stopUpdateCurrentTime()
    }

    fun onChangeConfig() {
        mediaPlayer?.pause()
        stopUpdateCurrentTime()
    }

    private fun releaseMediaPlayer() {
        mediaPlayer?.release()
        mediaPlayer = null
    }

    override fun onCleared() {
        super.onCleared()
        stopUpdateCurrentTime()
        releaseMediaPlayer()
    }
}