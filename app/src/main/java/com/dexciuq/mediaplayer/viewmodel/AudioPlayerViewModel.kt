package com.dexciuq.mediaplayer.viewmodel

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AudioPlayerViewModel : ViewModel() {

    private var mediaPlayer: MediaPlayer? = null
    private var playbackPosition: Int = 0

    private val _uri = MutableLiveData<Uri>()
    val uri: LiveData<Uri> = _uri

    private val _isPlaying = MutableLiveData<Boolean>()
    val isPlaying: LiveData<Boolean> = _isPlaying

    fun setAudioFile(context: Context, uri: Uri) {
        releaseMediaPlayer()
        mediaPlayer = MediaPlayer().apply {
            setDataSource(context, uri)
            prepare()
        }
        _uri.value = uri
        _isPlaying.value = false
        playbackPosition = 0
    }

    fun playAudio() {
        mediaPlayer?.apply {
            seekTo(playbackPosition)
            start()
        }
        _isPlaying.value = true
    }

    fun pauseAudio() {
        mediaPlayer?.apply {
            pause()
            playbackPosition = currentPosition
        }
        _isPlaying.value = false
    }

    private fun releaseMediaPlayer() {
        mediaPlayer?.release()
        mediaPlayer = null
    }

    override fun onCleared() {
        super.onCleared()
        releaseMediaPlayer()
    }
}