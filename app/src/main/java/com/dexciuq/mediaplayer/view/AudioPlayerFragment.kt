package com.dexciuq.mediaplayer.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.dexciuq.mediaplayer.databinding.FragmentAudioPlayerBinding
import com.dexciuq.mediaplayer.viewmodel.AudioPlayerViewModel

class AudioPlayerFragment : Fragment() {

    private val viewModel: AudioPlayerViewModel by viewModels()
    private val binding by lazy { FragmentAudioPlayerBinding.inflate(layoutInflater) }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding.playPause.setOnClickListener {
            if (viewModel.isPlaying.value == true) viewModel.pauseAudio()
            else viewModel.playAudio()
        }

        binding.chooseAudio.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                type = "audio/*"
                addCategory(Intent.CATEGORY_OPENABLE)
                putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("audio/*"))
            }
            audioPickerLauncher.launch(intent)
        }

        viewModel.uri.observe(viewLifecycleOwner) {
            it?.let {
                binding.filePath.text = "File Path: ${it.path}"
                binding.filePath.visibility = View.VISIBLE
                binding.playPause.visibility = View.VISIBLE
            } ?: run {
                binding.filePath.visibility = View.GONE
                binding.playPause.visibility = View.GONE
            }
        }

        viewModel.isPlaying.observe(viewLifecycleOwner) {
            binding.playPause.text = if (it) "Pause" else "Play"
        }

        return binding.root
    }

    private val audioPickerLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent: Intent? = result.data
                val fileUri: Uri? = intent?.data
                fileUri?.let { viewModel.setAudioFile(requireContext(), it) }
                return@registerForActivityResult
            }
            Toast.makeText(
                requireActivity(),
                "Cannot load audio",
                Toast.LENGTH_SHORT
            ).show()
        }
}