package com.dexciuq.mediaplayer.view

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.dexciuq.mediaplayer.databinding.FragmentVideoPlayerBinding
import com.dexciuq.mediaplayer.viewmodel.VideoPlayerViewModel


class VideoPlayerFragment : Fragment() {

    private val viewModel: VideoPlayerViewModel by viewModels()
    private val binding by lazy { FragmentVideoPlayerBinding.inflate(layoutInflater) }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding.videoView.setOnPreparedListener { mediaPlayer ->
            viewModel.setMediaPlayer(mediaPlayer)
            viewModel.currentTime.value?.let(viewModel::updateCurrentTime)
            if (viewModel.isPlaying.value == true) viewModel.playVideo()
        }

        binding.videoView.setOnCompletionListener {
            viewModel.complete()
        }

        binding.playPause.setOnClickListener {
            if (viewModel.isPlaying.value == true) viewModel.pauseVideo()
            else viewModel.playVideo()
        }

        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    binding.videoView.seekTo(progress)
                    viewModel.updateCurrentTime(progress)
                }
            }
            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        binding.chooseVideo.setOnClickListener {
            val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                type = "video/*"
                addCategory(Intent.CATEGORY_OPENABLE)
                putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("video/*"))
            }
            videoPickerLauncher.launch(intent)
        }

        viewModel.videoUri.observe(viewLifecycleOwner) {
            val views = listOf(binding.playPause, binding.seekBar, binding.videoView, binding.current, binding.duration)
            it?.let {
                binding.videoView.setVideoURI(it)
                views.forEach { view ->
                    view.visibility = View.VISIBLE
                }
            }
        }

        viewModel.currentTime.observe(viewLifecycleOwner) {
            binding.seekBar.progress = it
            binding.current.text = formatTime(it)
        }

        viewModel.duration.observe(viewLifecycleOwner) {
            binding.seekBar.max = it
            binding.duration.text = formatTime(it)
        }

        viewModel.isPlaying.observe(viewLifecycleOwner) {
            binding.playPause.text = if (it) "Pause" else "Play"
        }

        return binding.root
    }

    private val videoPickerLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val intent: Intent? = result.data
            val videoUri: Uri? = intent?.data
            videoUri?.let(viewModel::setVideoFile)
            return@registerForActivityResult
        }
        Toast.makeText(
            requireActivity(),
            "Cannot load video",
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun formatTime(timeInMillis: Int): String {
        val seconds = (timeInMillis / 1000) % 60
        val minutes = (timeInMillis / (1000 * 60)) % 60
        val hours = (timeInMillis / (1000 * 60 * 60)) % 24
        return String.format("%02d:%02d:%02d", hours, minutes, seconds)
    }

    override fun onPause() {
        viewModel.onChangeConfig()
        super.onPause()
    }
}